#include <iostream>
#include <fstream>
#include <vector>
#include <bitset>
#include <sstream>

using namespace std;

const u_int64_t genA = 16807;
const u_int64_t genB = 48271;
const u_int64_t factor = 2147483647;

void solveTask1(u_int64_t a, u_int64_t b) {
    int result = 0;
    for(int i = 0; i < 40000000; ++i) {
        a = (a * genA) % factor;
        b = (b * genB) % factor;
        bitset<16> lastA(a);
        bitset<16> lastB(b);
        if(lastA == lastB) {
            ++result;
        }
    }
    cout << "Task 1: " << result << endl;
}

void solveTask2(u_int64_t a, u_int64_t b) {
    int result = 0;
    for(int i = 0; i < 5000000; ++i) {
        while((a % 4) != 0) {
            a = (a * genA) % factor;
        }
        while((b % 8) != 0) {
            b = (b * genB) % factor;
        }
        bitset<16> lastA(a);
        bitset<16> lastB(b);
        if(lastA == lastB) {
            ++result;
        }
        a = (a * genA) % factor;
        b = (b * genB) % factor;
    }
    cout << "Task 2: " << result << endl;
}

int main(int argc, char** argv) {
    if(argc != 2) {
        cerr << "Bad arguments." << endl;
        return 1;
    }
    ifstream file(argv[1]);
     if(!file.is_open()) {
        cerr << "File failed to open!" << endl;
        return 2;
    }
    int a = 0, b = 0;
    string line;
    getline(file, line);
    a = stoi(line.substr(line.find_last_of(" ") + 1));
    getline(file, line);
    b = stoi(line.substr(line.find_last_of(" ") + 1));
    file.close();
    solveTask1(a, b);
    solveTask2(a, b);
    return 0;
}
