#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

void solveTask1(vector<int> vec) {
    int index = 0, jump = 0, steps = 0;
    while(true) {
        jump = vec[index];
        ++vec[index];
        index += jump;
        ++steps;
        if(index < 0 || index >= vec.size()) {
            break;
        }
    }
    cout << "Task 1: " << to_string(steps) << endl;
}

void solveTask2(vector<int> vec) {
    int index = 0, jump = 0, steps = 0;
    while(true) {
        jump = vec[index];
        jump < 3 ? ++vec[index] : --vec[index];
        index += jump;
        ++steps;
        if(index < 0 || index >= vec.size()) {
            break;
        }
    }
    cout << "Task 2: " << to_string(steps) << endl;
}

int main(int argc, char** argv) {
    if(argc != 2) {
        cerr << "Bad arguments." << endl;
        return 1;
    }
    ifstream file(argv[1]);
     if(!file.is_open()) {
        cerr << "File failed to open!" << endl;
        return 2;
    }
    vector<int> vec;
    string line;
    while(getline(file, line)) {
        vec.push_back(stoi(line));
    }
    file.close();

    solveTask1(vec);

    solveTask2(vec);
    return 0;
}
