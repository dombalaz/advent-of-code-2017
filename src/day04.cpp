#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

void solveTask1(ifstream &file) {
    bool dup = true;
    string line, token;
    vector<string> vec;
    int count = 0;
    while(getline(file, line)) {
        while(line.size()) {
            token = line.substr(0, line.find(" "));
            if(line.find(" ") != line.npos) {
                line = line.substr(line.find(" ") + 1);
            } else {
                line = "";
            }
            if(find(vec.begin(), vec.end(), token) != vec.end()) {
                dup = false;
                break;
            } else {
                vec.push_back(token);
            }
        }
        if(dup) {
            ++count;
        }
        dup = true;
        vec.clear();
    }
    cout << "Task 1: " << count << endl;
}

void solveTask2(ifstream &file) {
    bool valid = true;
    string line, token;
    vector<string> vec;
    int count = 0;
    while(getline(file, line)) {
        while(line.size()) {
            token = line.substr(0, line.find(" "));
            if(line.find(" ") != line.npos) {
                line = line.substr(line.find(" ") + 1);
            } else {
                line = "";
            }
            sort(token.begin(), token.end());
            if(find(vec.begin(), vec.end(), token) != vec.end()) {
                valid = false;
                break;
            } else {
                vec.push_back(token);
            }
        }
        if(valid) {
            ++count;
        }
        valid = true;
        vec.clear();
    }
    cout << "Task 2: " << count << endl;
}

int main(int argc, char** argv) {
    if(argc != 2) {
        cerr << "Bad arguments." << endl;
        return 1;
    }
    ifstream file(argv[1]);
     if(!file.is_open()) {
        cerr << "File failed to open!" << endl;
        return 2;
    }
     solveTask1(file);
    file.close();
    file.open(argv[1]);
    if(!file.is_open()) {
       cerr << "File failed to open!" << endl;
       return 2;
    }
    solveTask2(file);
    file.close();
    return 0;
}
