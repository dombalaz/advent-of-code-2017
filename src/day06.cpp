#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

int findMaxIndex(const vector<int> &vec) {
    int index = 0;
    for(unsigned int i= 0; i < vec.size(); ++i) {
        for(unsigned int j = i + 1; j < vec.size(); ++j) {
            if(vec.at(i) < vec.at(j)) {
                index = j;
                i = j;
            }
        }
        break;
    }
    return index;
}

void solveTask1(vector<int> &vec) {
    vector<vector<int>> check;
    check.push_back(vec);
    int steps = 0;
    while(true) {
        int maxIndex = findMaxIndex(vec);
        int maxValue = vec.at(maxIndex);
        vec[maxIndex] = 0;
        for(int i = 0; i < maxValue; ++i) {
            ++vec[++maxIndex % vec.size()];
        }
        ++steps;
        if(find(check.begin(), check.end(), vec) != check.end()) {
            break;
        } else {
            check.push_back(vec);
        }
    }
    cout << "Task 1: " << to_string(steps) << endl;
}

void solveTask2(vector<int> &vec) {
    vector<vector<int>> check;
    check.push_back(vec);
    int steps = 0;
    while(true) {
        int maxIndex = findMaxIndex(vec);
        int maxValue = vec.at(maxIndex);
        vec[maxIndex] = 0;
        for(int i = 0; i < maxValue; ++i) {
            ++vec[++maxIndex % vec.size()];
        }
        ++steps;
        if(find(check.begin(), check.end(), vec) != check.end()) {
            break;
        } else {
            check.push_back(vec);
        }
    }
    cout << "Task 2: " << to_string(steps) << endl;
}

int main(int argc, char** argv) {
    if(argc != 2) {
        cerr << "Bad arguments." << endl;
        return 1;
    }
    ifstream file(argv[1]);
     if(!file.is_open()) {
        cerr << "File failed to open!" << endl;
        return 2;
    }
    vector<int> vec;
    string line, token;
    getline(file, line);
    file.close();
    while(!line.empty()) {
        token = line.substr(0, line.find("\t"));
        line.find("\t") != line.npos ? line = line.substr(line.find("\t") + 1) : line = "";
        vec.push_back(stoi(token));
    }
    solveTask1(vec);

    solveTask2(vec);
    return 0;
}
