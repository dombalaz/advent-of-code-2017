#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

void solveTasks(string &line) {
    string token;
    float x = 0, y = 0, max = 0;
    while(line.size()) {
        token = line.substr(0, line.find(","));
        line = line.find(",") != line.npos ? line.substr(line.find(",") + 1) : "";
        if(token == "n") {
            y += 1;
        } else if(token == "ne") {
            x += 0.5;
            y += 0.5;
        } else if(token == "se") {
            x += 0.5;
            y -= 0.5;
        } else if(token == "s") {
            y -= 1;
        } else if(token == "sw") {
            x -= 0.5;
            y -= 0.5;
        } else if(token == "nw"){
            x -= 0.5;
            y += 0.5;
        }
        float checkY = floor(y), result;
        if(checkY != y || y != 0) {
            result = abs(x) + abs(y);
        } else {
            result = abs(x) * 2 + abs(y);
        }
        if(max < result) {
            max = result;
        }
    }
    float checkY = floor(y), result;
    if(checkY != y || y != 0) {
        result = abs(x) + abs(y);
    } else {
        result = abs(x) * 2 + abs(y);
    }
    cout << "Task 1: " << to_string((int) result) << endl;
    cout << "Task 2: " << to_string((int) max) << endl;
}

int main(int argc, char** argv) {
    if(argc != 2) {
        cerr << "Bad arguments." << endl;
        return 1;
    }
    ifstream file(argv[1]);
     if(!file.is_open()) {
        cerr << "File failed to open!" << endl;
        return 2;
    }
    string line;
    getline(file, line);
    file.close();
    solveTasks(line);
    return 0;
}
