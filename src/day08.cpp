#include <iostream>
#include <fstream>
#include <map>

using namespace std;

bool eligible(const int left,const  string &operation, const int right) {
    if(operation == "==") {
        if(left == right) {
            return true;
        }
    } else if(operation == "!=") {
        if(left != right) {
            return true;
        }
    } else if(operation == "<") {
        if(left < right) {
            return true;
        }
    } else if(operation == ">") {
        if(left > right) {
            return true;
        }
    } else if(operation == "<=") {
        if(left <= right) {
            return true;
        }
    } else {
        if(left >= right) {
            return true;
        }
    }
    return false;
}

int getMax(const map<string, int> &registers) {
    auto beg = *registers.begin();
    int max = beg.second;
    for(auto it : registers) {
        if(max < it.second) {
            max = it.second;
        }
    }
    return max;
}

void solveTasks(ifstream &file) {
    map<string, int> registers;
    int value1 = 0, value2 = 0, max = 0;
    string line, reg1, reg2, operation1, operation2;
    while(getline(file, line)) {
        reg1 = line.substr(0, line.find(" "));
        line = line.substr(line.find(" ") + 1);
        operation1 = line.substr(0, line.find(" "));
        line = line.substr(line.find(" ") + 1);
        value1 = stoi(line.substr(0, line.find(" ")));
        line = line.substr(line.find(" ") + 4);
        reg2 = line.substr(0, line.find(" "));
        line = line.substr(line.find(" ") + 1);
        operation2 = line.substr(0, line.find(" "));
        value2 = stoi(line.substr(line.find(" ") + 1));
        if(registers.count(reg1) == 0) {
            registers.insert(pair<string, int>(reg1, 0));
        }
        if(registers.count(reg2) == 0) {
            registers.insert(pair<string, int>(reg2, 0));
        }
        if(eligible(registers.at(reg2), operation2, value2)) {
            registers[reg1] += operation1 == "inc" ? value1 : value1 * -1;
            int currentMax = getMax(registers);
            if(currentMax > max) {
                max = currentMax;
            }
        }
    }
    cout << "Task 1: " << to_string(getMax(registers)) << endl;
    cout << "Task 2: " << to_string(max) << endl;
}

int main(int argc, char** argv) {
    if(argc != 2) {
        cerr << "Bad arguments." << endl;
        return 1;
    }
    ifstream file(argv[1]);
     if(!file.is_open()) {
        cerr << "File failed to open!" << endl;
        return 2;
    }
    solveTasks(file);
    file.close();
    return 0;
}
