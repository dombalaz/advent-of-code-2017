#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <sstream>

using namespace std;

string computeKnotHash(const string &lineCopy) {
    vector<char> seq;
    for(unsigned int i = 0; i < lineCopy.size(); ++i) {
        seq.push_back(lineCopy.at(i));;
    }

    seq.push_back(17);
    seq.push_back(31);
    seq.push_back(73);
    seq.push_back(47);
    seq.push_back(23);
    vector<unsigned char> vec;
    vec.clear();
    int skip = 0;
    int id = 0;
    int tmp = 0;
    for(int i = 0; i < 256; ++i) {
        vec.push_back(i);
    }
    for(int l = 0; l < 64; ++l) {
        for(auto it : seq) {
            for(int i = id, j = id + it - 1, k = 0; k < it / 2; ++i, --j, ++k) {
                tmp = vec[i % vec.size()];
                vec[i % vec.size()] = vec[j % vec.size()];
                vec[j % vec.size()] = tmp;
            }
            id = (id + it + skip) % vec.size();
            ++skip;
        }
    }
    vector<unsigned char> hash;
    int it = 0;
    for(int i = 0; i < 16; ++i) {
        unsigned char chash = 0;
        for(int j = 0; j < 16; ++j, ++it) {
            chash ^= vec.at(it);
        }
        hash.push_back(chash);
    }
    stringstream result;
    for(auto it : hash) {
        result << setfill('0') << setw(2) << hex << (int)it;
    }
    return result.str();
}

void solveTasks(string line) {
    string lineCopy(line);
    vector<int> vec;
    int input = 0, skip = 0, id = 0, tmp = 0;
    for(int i = 0; i < 256; ++i) {
        vec.push_back(i);
    }
    while(line.size()) {
        input = line.find(",") != line.npos ? stoi(line.substr(0, line.find(","))) : stoi(line);
        line = line.find(",") != line.npos ? line.substr(line.find(",") + 1) : "";
        for(int i = id, j = id + input - 1, k = 0; k < input / 2; ++i, --j, ++k) {
            tmp = vec[i % vec.size()];
            vec[i % vec.size()] = vec[j % vec.size()];
            vec[j % vec.size()] = tmp;
        }
        id = (id + input + skip) % vec.size();
        ++skip;
    }
    cout << "Task 1: " << vec.at(0) * vec.at(1) << endl;
    cout << "Task 2: " << computeKnotHash(lineCopy) << endl;
}

int main(int argc, char** argv) {
    if(argc != 2) {
        cerr << "Bad arguments." << endl;
        return 1;
    }
    ifstream file(argv[1]);
     if(!file.is_open()) {
        cerr << "File failed to open!" << endl;
        return 2;
    }
    string line;
    getline(file, line);
    file.close();
    solveTasks(line);
    return 0;
}
