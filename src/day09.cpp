#include <iostream>
#include <fstream>

using namespace std;

void solveTasks(ifstream &file) {
    char c = 0;
    int countGroup = 0, result1 = 0, result2 = 0;
    bool garbage = false, ignore = false;
    while(file.good()) {
        c = file.get();
        if(c == '!' && !ignore) {
            ignore = true;
            continue;
        } else if(ignore) {
            ignore = false;
            continue;
        }
        if(garbage) {
            if(c == '>') {
                garbage = false;
                continue;
            }
            ++result2;
            continue;
        } else {
            switch(c) {
            case '{' :
                ++countGroup;
                break;
            case '}':
                result1 += countGroup;
                --countGroup;
                break;
            case '<':
                garbage = true;
                break;
            }
        }
    }
    cout << "Task 1: " << to_string(result1) << endl;
    cout << "Task 2: " << to_string(result2) << endl;
}

int main(int argc, char** argv) {
    if(argc != 2) {
        cerr << "Bad arguments." << endl;
        return 1;
    }
    ifstream file(argv[1]);
     if(!file.is_open()) {
        cerr << "File failed to open!" << endl;
        return 2;
    }
    solveTasks(file);
    file.close();
    return 0;
}
