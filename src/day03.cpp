#include <iostream>
#include <fstream>
#include <map>

using namespace std;

void solveTask1(int input) {
    bool increment = true;
    int steps = 0, stepsDif = 0, stepsCount = 0, levelSide = 1, levelMax = 1, levelCount = 1;
    for(int i = 1; i < input; ++i) {
        for(;levelCount < levelMax && i < input; ++i, ++levelCount) {
            if(increment) {
                ++steps;
            } else {
                --steps;
            }
            ++stepsCount;
            if(stepsCount == stepsDif) {
                increment = !increment;
                stepsCount = 0;
            }
        }
        if(i < input) {
            levelSide += 2;
            levelMax = 4 * levelSide - 4;
            levelCount = 0;
            steps += 1;
            stepsDif += 1;
            stepsCount = 0;
        }
    }
    cout << "Task 1: " << to_string(steps) << endl;
}

int countValue(const map<pair<int, int>, int> &grid, int x, int y) {
    int result = 0;
    --x;
    --y;
    for(int i = 0; i < 3; ++i, ++x) {
        for(int j = 0; j < 3; ++j, ++y) {
            if(grid.count(pair<int, int>(x, y))) {
                result += grid.at(pair<int, int>(x, y));
            }
        }
        y -= 3;
    }
    return result;
}

void solveTask2(int input) {
    map<pair<int, int>, int> grid;
    grid[pair<int, int>(0, 0)] = 1;

    int x = 1, y = -1, direction = 0,  levelSide = 2;
    while(true) {
        for(int i = 0; i < 4; ++i) {
            for(int i = 0; i < levelSide; ++i) {
                switch (direction % 4) {
                    case 0:
                        ++y;
                        break;
                    case 1:
                        --x;
                        break;
                    case 2:
                        --y;
                        break;
                    case 3:
                        ++x;
                        break;
                }
                grid[pair<int, int>(x, y)] = countValue(grid, x, y);
                if(grid.at(pair<int, int>(x, y)) > input) {
                    cout << "Task 2: " << to_string(grid.at(pair<int, int>(x, y))) << endl;
                    return;
                }
            }
            direction = (direction + 1) % 4;
        }
        ++x;
        --y;
        levelSide += 2;
    }
}

int main(int argc, char** argv) {
    if(argc != 2) {
        cerr << "Bad arguments." << endl;
        return 1;
    }
    ifstream file(argv[1]);
     if(!file.is_open()) {
        cerr << "File failed to open!" << endl;
        return 2;
    }
    string s;
    getline(file, s);
    file.close();
    solveTask1(stoi(s));
    solveTask2(stoi(s));
    return 0;
}
