#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <set>
#include <algorithm>

using namespace std;

void solveTasks(ifstream &file) {
    map<int, vector<int>> groups;
    int group = 0;
    string line;
    while(getline(file, line)) {
        group = stoi(line.substr(0, line.find(" ")));
        groups.insert(pair<int, vector<int>>(group, vector<int>()));
        line = line.substr(line.find(" ") + 5);
        while(line.size()) {
            groups[group].push_back(stoi(line.substr(0, line.find(","))));
            line = line.find(",") != line.npos ? line.substr(line.find(",") + 2) : "";
        }
    }
    bool task1 = true;
    int task2 = 0;
    while(groups.size()) {
        set<int> result({(*groups.begin()).first}), tmp({(*groups.begin()).first});
        result.insert((*groups.begin()).second.begin(), (*groups.begin()).second.end());
        while(true) {
            for(auto it : groups) {
                for(int j : groups[it.first]) {
                    if(find(result.begin(), result.end(), j) != result.end()) {
                        tmp.insert(it.first);
                        break;
                    }
                }
            }
            if(tmp.size() > result.size()) {
                result = tmp;
            } else {
                break;
            }
        }
        if(task1) {
            cout << "Task 1: " << to_string(result.size()) << endl;
            task1 = false;
        }
        ++task2;
        for(int i : result) {
            groups.erase(i);
        }
        result.clear();
        tmp.clear();
    }
    cout << "Task 2: " << to_string(task2) << endl;
}

int main(int argc, char** argv) {
    if(argc != 2) {
        cerr << "Bad arguments." << endl;
        return 1;
    }
    ifstream file(argv[1]);
     if(!file.is_open()) {
        cerr << "File failed to open!" << endl;
        return 2;
    }
    solveTasks(file);
    return 0;
}
