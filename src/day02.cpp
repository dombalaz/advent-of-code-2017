#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

void solveTask1(ifstream &file) {
    string line;
    string delim = "\t";
    string token;
    int sum = 0;
    while(getline(file, line)) {
        token = line.substr(0, line.find(delim));
        vector<int> vec;
        while(!token.empty()) {
            vec.push_back(stoi(token));
            if(line.find(delim) != line.npos) {
                line = line.substr(line.find(delim) + 1);
                token = line.substr(0, line.find(delim));
            } else {
                token = "";
            }
        }
        sort(vec.begin(), vec.end());
        sum += vec.at(vec.size() - 1) - vec.at(0);
    }
    cout << "Task 1: " << to_string(sum) << endl;
}

void solveTask2(ifstream &file) {
    file.seekg(0);
    string line;
    string delim = "\t";
    string token;
    int sum = 0;
    while(getline(file, line)) {
        token = line.substr(0, line.find(delim));
        vector<int> vec;
        while(!token.empty()) {
            vec.push_back(stoi(token));
            if(line.find(delim) != line.npos) {
                line = line.substr(line.find(delim) + 1);
                token = line.substr(0, line.find(delim));
            } else {
                token = "";
            }
        }
        sort(vec.begin(), vec.end(), greater<int>());
        for(unsigned int i = 0; i < vec.size(); ++i) {
            for(unsigned int j = i + 1; j < vec.size(); ++j) {
                if(vec.at(i) % vec.at(j) == 0) {
                    sum += vec.at(i) / vec.at(j);
                    break;
                }
            }
        }
    }
    cout << "Task 2: " << to_string(sum) << endl;
}


int main(int argc, char** argv) {
    if(argc != 2) {
        cerr << "Bad arguments." << endl;
        return 1;
    }
    ifstream file(argv[1]);
     if(!file.is_open()) {
        cerr << "File failed to open!" << endl;
        return 2;
    }
    solveTask1(file);
    file.close();
    file.open(argv[1]);
    if(!file.is_open()) {
       cerr << "File failed to open!" << endl;
       return 2;
   }
    solveTask2(file);
    file.close();
    return 0;
}
