#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

const u_int64_t total = 1000000000;
const string start = "abcdefghijklmnop";

void spin(string &s, int n) {
    n %= s.size();
    for(int i = 0; i < n; ++i) {
        char tmp = s[s.size() - 1];
        for(int j = 0; j < s.size(); ++j) {
            char c = s[j];
            s[j] = tmp;
            tmp = c;
        }
    }
}

void exchange(string &s, int a, int b) {
    char tmp = s[a];
    s[a] = s[b];
    s[b] = tmp;
}

void partner(string &s, char a, char b) {
    int posA = s.find(a);
    int posB = s.find(b);
    exchange(s, posA, posB);
}

void dance(string &programs, string input) {
    string token;
    while(!input.empty()) {
        token = input.substr(0, input.find(","));
        input = input.find(",") != input.npos ? input.substr(input.find(",") + 1) : "";
        if(token[0] == 's') {
            spin(programs, stoi(token.substr(1)));
        } else if(token[0] == 'x') {
            exchange(programs, stoi(token.substr(1, token.find("/"))), stoi(token.substr(token.find("/") + 1)));
        } else {
            partner(programs, token[1], token[3]);
        }
    }
}

void solveTask1(string input) {
    string programs = start;
    dance(programs, input);
    cout << "Task 1: " << programs << endl;
}

void solveTask2(string input) {
    string programs = start;
    u_int64_t c = 0;
    for(u_int64_t i = 0; i < total; ++i, ++c) {
        dance(programs, input);
        if(programs == start) {
            break;
        }
    }
    c = (total % c) + 1;
    programs = start;
    for(u_int64_t i = 0; i < c; ++i) {
        dance(programs, input);
    }
    cout << "Task 2: " <<  programs << endl;
}

int main(int argc, char** argv) {
    if(argc != 2) {
        cerr << "Bad arguments." << endl;
        return 1;
    }
    ifstream file(argv[1]);
     if(!file.is_open()) {
        cerr << "File failed to open!" << endl;
        return 2;
    }
    string input;
    getline(file, input);
    file.close();
    solveTask1(input);
    solveTask2(input);
    return 0;
}
