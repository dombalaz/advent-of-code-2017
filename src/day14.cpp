#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <sstream>
#include <bitset>

using namespace std;

string computeKnotHash(const string &lineCopy) {
    vector<char> seq;
    for(unsigned int i = 0; i < lineCopy.size(); ++i) {
        seq.push_back(lineCopy.at(i));;
    }

    seq.push_back(17);
    seq.push_back(31);
    seq.push_back(73);
    seq.push_back(47);
    seq.push_back(23);
    vector<unsigned char> vec;
    vec.clear();
    int skip = 0;
    int id = 0;
    int tmp = 0;
    for(int i = 0; i < 256; ++i) {
        vec.push_back(i);
    }
    for(int l = 0; l < 64; ++l) {
        for(auto it : seq) {
            for(int i = id, j = id + it - 1, k = 0; k < it / 2; ++i, --j, ++k) {
                tmp = vec[i % vec.size()];
                vec[i % vec.size()] = vec[j % vec.size()];
                vec[j % vec.size()] = tmp;
            }
            id = (id + it + skip) % vec.size();
            ++skip;
        }
    }
    vector<unsigned char> hash;
    int it = 0;
    for(int i = 0; i < 16; ++i) {
        unsigned char chash = 0;
        for(int j = 0; j < 16; ++j, ++it) {
            chash ^= vec.at(it);
        }
        hash.push_back(chash);
    }
    stringstream result;
    for(auto it : hash) {
        result << setfill('0') << setw(2) << hex << (int)it;
    }
    return result.str();
}

vector<string> solveTask1(string line) {
    vector<string> hashFieldString;
    vector<string> resultField;
    for(int i = 0; i < 128; ++i) {
        hashFieldString.push_back(computeKnotHash(line + "-" + to_string(i)));
    }
    string tmp;
    int result = 0;
    for(const string & hash : hashFieldString) {
        for(const char &c : hash) {
            unsigned int x;
            stringstream ss;
            ss << std::hex << c;
            ss >> x;
            tmp.append(bitset<4>(x).to_string());
        }
        resultField.push_back(tmp);
        for(unsigned int i = 0; i < tmp.size(); ++i) {
            if(tmp.at(i) == '1') {
                ++result;
            }
        }
        tmp.clear();
    }
    cout << "Task 1: " << result << endl;
    return resultField;
}

bool processGroup(vector<string> &grid ,int x, int y) {
    if(x < 0 || y < 0 || x >= grid.size() || y >= grid.size()) {
        return false;
    }
    if(grid.at(x).at(y) == '1') {
        grid[x][y] = '0';
        processGroup(grid, x, y - 1);
        processGroup(grid, x, y + 1);
        processGroup(grid, x - 1, y);
        processGroup(grid, x + 1, y);
        return true;
    } else {
        return false;
    }
}

void solveTask2(string line) {
    vector<string> hashField = solveTask1(line);
    int result = 0;
    for(int i = 0; i < hashField.size(); ++i) {
        for(int j = 0; j < hashField.size(); ++j) {
            if(processGroup(hashField, i, j)) {
                ++result;
            }
        }
    }
    cout << "Task 2: " << result << endl;
}

int main(int argc, char** argv) {
    if(argc != 2) {
        cerr << "Bad arguments." << endl;
        return 1;
    }
    ifstream file(argv[1]);
     if(!file.is_open()) {
        cerr << "File failed to open!" << endl;
        return 2;
    }
    string line;
    getline(file, line);
    file.close();
    solveTask2(line);
    return 0;
}
