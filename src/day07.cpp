#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;
int diff = 0;


class Node {
public:
    Node(int weight) :
        parent_(nullptr),
        weight_(weight) {}

    void addChild(Node *node) {
        if(find(childrens_.begin(), childrens_.end(), node) == childrens_.end()) {
            childrens_.push_back(node);
        }
    }

    void setParent(Node *node) {
        parent_ = node;
    }

    const Node *getParent() const {
        return parent_;
    }

    const vector<Node*> &childrens() const {
        return childrens_;
    }

    int getWeight() const {
        return weight_;
    }

    void setWeight(int weight) {
        weight_ = weight;
    }

private:
    vector<Node*> childrens_;
    Node *parent_;
    int weight_;
};

void solveTask1(ifstream &file, map<string, Node*> &nodes) {
    string line, parent;
    while(getline(file, line)) {
        nodes[line.substr(0, line.find(" "))] = new Node(stoi(line.substr(line.find("(") + 1, line.find(")"))));
    }
    file.clear();
    file.seekg(0, ios::beg);
    while(getline(file, line)) {
        parent = line.substr(0, line.find(" "));
        line = line.find(">") != line.npos ? line.substr(line.find(">") + 2) : "";
        while(line.size()) {
            nodes[line.substr(0, line.find(","))]->setParent(nodes[parent]);
            nodes[parent]->addChild(nodes[line.substr(0, line.find(","))]);
            line = line.find(",") != line.npos ? line.substr(line.find(",") + 2) : "";
        }
    }
    string result;
    for(auto it : nodes) {
        if(it.second->getParent() == nullptr) {
            result = it.first;
        }
    }
    cout << "Task 1: " << result << endl;
}

int getTotalWeight(const Node *node) {
    if(node->childrens().size()) {
        int result = node->getWeight();
        for(const Node *it : node->childrens()) {
            result += getTotalWeight(it);
        }
        return result;
    } else {
        return node->getWeight();
    }
}

void evaluate(const Node *node) {
    vector<int> weights;
    for(auto it : node->childrens()) {
        weights.push_back(getTotalWeight(it));
    }
    int id = -1;
    for(unsigned int i = 0; i < weights.size() - 1; ++i) {
        if(weights.at(i) != weights.at(i + 1)) {
            if(i + 2 < weights.size()){
                if(weights.at(i) != weights.at(i + 2)) {
                    id = i;
                } else {
                    id = i + 1;
                }
            } else {
                if(i == 0) {
                    id = i;
                } else {
                    id = i + 1;
                }
            }
        }
    }
    if(id == -1) {
        cout << "Task 2: " << to_string(node->getWeight() + diff) << endl;
        return;
    } else {
        evaluate(node->childrens().at(id));
    }
}

void solveTask2(map<string, Node*> &nodes) {
    string r;
    for(auto it : nodes) {
        if(it.second->getParent() == nullptr) {
            r = it.first;
            break;
        }
    }
    Node *root = nodes[r];
    vector<int> weights;
    for(auto it : root->childrens()) {
        weights.push_back(getTotalWeight(it));
    }
    sort(weights.begin(), weights.end());
    if(weights.at(0) == weights.at(1)) {
        diff = weights.at(0) - weights.at(weights.size() - 1);
    } else {
        diff = weights.at(0) - weights.at(1);
    }
    evaluate(root);
}

int main(int argc, char** argv) {
    if(argc != 2) {
        cerr << "Bad arguments." << endl;
        return 1;
    }
    ifstream file(argv[1]);
     if(!file.is_open()) {
        cerr << "File failed to open!" << endl;
        return 2;
    }
    map<string, Node*> nodes;
    solveTask1(file, nodes);
    file.close();
    solveTask2(nodes);

    for(auto it : nodes) {
        delete it.second;
    }

    return 0;
}
