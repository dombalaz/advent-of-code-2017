#include <iostream>
#include <fstream>
#include <map>

using namespace std;

void solveTask1(const map<int, int> layers, const unsigned int top) {
    int result = 0;
    for(unsigned int i = 1; i <= top; ++i) {
        if(layers.count(i)) {
            if(i % layers.at(i) == 0) {
                result += i * (layers.at(i) / 2 + 1);
            }
        }
    }
    cout << "Task 1: " << to_string(result) << endl;
}

bool solveTask2(const map<int, int> &layers, const int top, const int delay) {
    for(int i = 0; i <= top; ++i) {
        if(layers.count(i)) {
            if((i + delay) % layers.at(i) == 0) {
                return false;
            }
        }
    }
    cout << "Task 2: " << to_string(delay) << endl;
    return true;
}

void solveTasks(ifstream &file) {
    map<int, int> layers;
    string line;
    int first = 0, second = 0;
    while(getline(file, line)) {
        first = stoi(line.substr(0, line.find(":")));
        second = stoi(line.substr(line.find(" ") + 1));
        layers.insert(pair<int, int>(first, (second - 1) * 2));
    }
    solveTask1(layers, first);

    int delay = 0;
    while(!solveTask2(layers, first, delay++)) {}
}

int main(int argc, char** argv) {
    if(argc != 2) {
        cerr << "Bad arguments." << endl;
        return 1;
    }
    ifstream file(argv[1]);
     if(!file.is_open()) {
        cerr << "File failed to open!" << endl;
        return 2;
    }
    solveTasks(file);
    return 0;
}
