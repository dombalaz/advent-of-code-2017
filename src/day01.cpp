#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char** argv) {
    if(argc != 2) {
        cerr << "Bad arguments." << endl;
        return 1;
    }
    ifstream file(argv[1]);
     if(!file.is_open()) {
        cerr << "File failed to open!" << endl;
        return 2;
    }
    string line;
    getline(file, line);
    file.close();

    int sum = 0;
    for(unsigned int i = 0; i < line.size() - 1; ++i) {
        if(line.at(i) == line.at(i + 1)) {
            sum += line.at(i) -'0';
        }
    }
    if(line.at(0) == line.at(line.size() - 1)) {
        sum += line.at(0) - '0';
    }
    cout << "Task 1: " + to_string(sum) << endl;

    sum = 0;
    int steps = line.size() / 2;
    for(unsigned int i = 0; i < line.size(); ++i) {
        if(line.at(i) == line.at((i + steps) % line.size())) {
            sum += line.at(i) - '0';
        }
    }
    cout << "Task 2: " + to_string(sum) << endl;

    return 0;
}
