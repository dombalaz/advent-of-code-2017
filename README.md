# Advent of Code 2017
This project is for solving Advent of Code 2017 problems from the website [Advent of Code](http://adventofcode.com/2017). These are my sollutions not official. It's very funny challenge, so try to make up your own solution before looking at my not perfect code ;).

## Dependencies
- C++ compiler supporting C++11.
- [CMake](https://cmake.org/) at least v2.8.0

## Currently solved problems
- Day 01
- ...
- Day 15

## Building from sources
In project directory run
```
$ mkdir build
$ cd build
$ cmake ../
$ make
```

## Build just some days
If you would like to exclude some days from building you can run CMake with these options
- `$ cmake -D BUILD_DAY="<xy>" ../`
- `$ cmake -D DONT_BUILD_DAY="<xy>" ../`

where xy is number of day and days are separated with spaces.

## Run binaries
```
$ ./dayXX <path-to-input-file>
```

